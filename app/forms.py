from flask_wtf import FlaskForm
from flask_ckeditor import CKEditorField
from wtforms import StringField, TextField, TextAreaField,  PasswordField, BooleanField, SubmitField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from app.models import User
import smtplib
import ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from app import app
from app.utils import remove_html_tags
from flask import request


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')


class ComposeForm(FlaskForm):
    recipient = StringField('Recipient', validators=[DataRequired(), Email()])
    subject = TextField('Subject', validators=[DataRequired()])
    body = CKEditorField('Body', validators=[DataRequired()])
    submit = SubmitField('Send')

    def send_mail(self):

        port = app.config['MAIL_PORT']  # For starttls
        smtp_server = app.config['MAIL_SERVER']
        sender_email = app.config['MAIL_USERNAME']
        receiver_email = self.recipient.data
        password = app.config['MAIL_PASSWORD']
        message = MIMEMultipart("alternative")
        message["Subject"] = self.subject.data
        message["From"] = sender_email
        message["To"] = receiver_email
        text = remove_html_tags(self.body.data)
        html = self.body.data
        part1 = MIMEText(text, 'plain')
        part2 = MIMEText(html, "html")
        message.attach(part1)
        message.attach(part2)
        # message = """
        # Subject: {subject}

        # {body}""".format(subject=self.subject.data, body=self.body.data)

        context = ssl.create_default_context()
        with smtplib.SMTP(smtp_server, port) as server:
            server.ehlo()  # Can be omitted
            server.starttls(context=context)
            server.ehlo()  # Can be omitted
            server.login(sender_email, password)
            server.sendmail(sender_email, receiver_email, message.as_string())


class SearchForm(FlaskForm):
    q = StringField('Search', validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)


class IndexForm(FlaskForm):
    read = BooleanField('Read it?')
    