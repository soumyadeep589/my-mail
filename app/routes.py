from flask import render_template, flash, redirect, url_for, request, g
from werkzeug.urls import url_parse
from app import app, db, celery
from app.forms import LoginForm, RegistrationForm, ComposeForm, SearchForm, IndexForm
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User, Mail
import os



@app.before_request
def before_request():
    if current_user.is_authenticated:
        db.session.commit()
        g.search_form = SearchForm()


@app.route('/')
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index():

    page = request.args.get('page', 1, type=int)
    form = IndexForm()
    # if form.validate_on_submit():
    #     print('HEllo')
    #     for mail in Mail.query.all():
    #         print('5')
    #     # mail = Mail.query.filter_by(id=my_id)
    #     # mail.checked_in = form.read.data
    #     # user.set_password(form.password.data)
    #     # print(request['read'])

    #     # db.session.commit()
    #     return redirect(url_for('index'))

    mails = Mail.query.filter_by(recipient=current_user.email).order_by(Mail.timestamp.desc()).paginate(page, app.config['MAILS_PER_PAGE'], False)
    next_url = url_for('index', page=mails.next_num) \
        if mails.has_next else None
    prev_url = url_for('index', page=mails.prev_num) \
        if mails.has_prev else None
    return render_template('index.html', title='Home', mails=mails.items, next_url=next_url, prev_url=prev_url, form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('index'))
    return render_template('register.html', title='Register', form=form)


# @app.route('/user/<username>')
# @login_required
# def user(username):
#     user = User.query.filter_by(username=username).first_or_404()
#     mails = [
#         {'sender': user, 'body': 'Test post #1'},
#         {'sender': user, 'body': 'Test post #2'}
#     ]
#     return render_template('user.html', user=user, mails=mails)


@app.route('/compose', methods=['GET', 'POST'])
@login_required
def compose():
    form = ComposeForm()
    if form.validate_on_submit():
        mail = Mail(recipient=form.recipient.data, subject=form.subject.data, body=form.body.data, sender=current_user.email)
        # user.set_password(form.password.data)
        form.send_mail()
        db.session.add(mail)
        db.session.commit()

        flash('Congratulations, your email has been sent!')
        return redirect(url_for('index'))
    return render_template('compose.html', title='Compose', form=form)


@app.route('/sentbox')
@login_required
def sentbox():
    page = request.args.get('page', 1, type=int)
    mails = Mail.query.filter_by(sender=current_user.email).order_by(Mail.timestamp.desc()).paginate(page, app.config['MAILS_PER_PAGE'], False)
    next_url = url_for('sentbox', page=mails.next_num) \
        if mails.has_next else None
    prev_url = url_for('sentbox', page=mails.prev_num) \
        if mails.has_prev else None
    return render_template('sentbox.html', title='Sentbox', mails=mails.items, next_url=next_url, prev_url=prev_url)


@app.route('/inbox/<mailid>')
@login_required
def get_mail(mailid):
    mails = Mail.query.filter_by(sender=mailid, recipient=current_user.email).order_by(Mail.timestamp.desc())
    return render_template('specmail.html', title='Specmail', mails=mails)


@app.route('/search')
@login_required
def search():
    if not g.search_form.validate():
        return redirect(url_for('index'))
    page = request.args.get('page', 1, type=int)
    mails, total = Mail.search(g.search_form.q.data, page,
                               app.config['MAILS_PER_PAGE'])
    next_url = url_for('search', q=g.search_form.q.data, page=page + 1) \
        if total > page * app.config['MAILS_PER_PAGE'] else None
    prev_url = url_for('search', q=g.search_form.q.data, page=page - 1) \
        if page > 1 else None
    return render_template('search.html', title='Search', mails=mails,
                           next_url=next_url, prev_url=prev_url)


@app.route('/generate_file')
@login_required
def generate_file():
    task_id = create_log.delay(current_user.id)
    print(task_id)
    return {'id': str(task_id)}


@celery.task(name="tasks.create_log")
def create_log(id):
    current_user = User.query.get(id)
    mails = Mail.query.filter_by(recipient=current_user.email)
    transactions_text = "Name : {}\n".format(current_user.username)
    for t in mails:
        transactions_text += f'''
        mail_id: {t.sender}
        recipient : {t.recipient}
        subject : {t.subject}
        body: {t.body}
        ========================================'''
    file_path = os.path.join(os.getcwd(), "app", "static", "{}.txt".format(current_user.id))
    if os.path.exists(file_path):
        os.remove(file_path)
    with open(file_path, "w") as f:
        f.write(transactions_text)
    # return {"url" : "/static/{}.txt".format(current_user.id)}
    url = "/static/{}.txt".format(current_user.id)

    # file_location = "/static/{}.txt".format(current_user.id)
    return url


@app.route('/get_file/<id>')
def get_file(id):
    task = celery.AsyncResult(id)
    print(task)
    url = task.result
    # print(url)
    return {'url': url}


@app.route('/index/<id>', methods=['GET', 'POST'])
def read(id):
    print(id)
    # return {'id': id}

    mail = Mail.query.filter_by(id=id)
    mail.checked_in = True
    mail.update({Mail.checked_in: True}, synchronize_session=False)
    db.session.commit()
    return redirect(url_for('index'))

