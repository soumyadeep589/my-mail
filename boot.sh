#!/bin/sh
source flask-mymail/bin/activate
flask db upgrade
exec gunicorn -b :5000 --access-logfile - --error-logfile - my_mail:app