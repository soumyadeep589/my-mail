FROM python:3.6-alpine

RUN adduser -D my_mail

WORKDIR /home/my_mail

COPY requirements.txt requirements.txt
RUN python -m venv flask-mymail
RUN flask-mymail/bin/pip install --upgrade pip
RUN flask-mymail/bin/pip install -r requirements.txt
RUN flask-mymail/bin/pip install gunicorn

COPY app app
COPY migrations migrations
COPY my_mail.py config.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP my_mail.py

RUN chown -R my_mail:my_mail ./
USER my_mail

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]